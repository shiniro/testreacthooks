import React, { useReducer, useEffect, useCallback, useMemo } from 'react';

import IngredientForm from './IngredientForm';
import IngredientList from './IngredientList';
import Search from './Search';
import ErrorModal from '../UI/ErrorModal';

import useHttp from '../../hooks/http';

const ingredientReducer = (curentIngredients, action) => {
  switch (action.type) {
    case 'SET':
      return action.ingredients;
    case 'ADD':
      return [...curentIngredients, action.ingredient];
    case 'DELETE':
      return curentIngredients.filter(ingredient => ingredient.id !== action.id);
    default:
      throw new Error('Should not get there !!! Ingredient');
  }
}

const Ingredients = () => {

  const [ userIngredients, dispatch ] = useReducer(ingredientReducer, []); // second argument initial state
  const { isLoading, data, error, sendRequest, reqExtra, reqIndentifier, clear } = useHttp();

  // const [ userIngredients, setUserIngredients ] = useState([]);
  // const [ isLoading, setIsLoading ] = useState(false);
  // const [ error, setError ] = useState();

  // after every render cycle
  // useEffect(() => {
  //   fetch('https://react-hooks-update-c3e42.firebaseio.com/ingredients.json')
  //   .then(response => {
  //     return response.json();
  //   }).then (responseData => {
  //     const loadedIngredients = [];
  //     for (const key in responseData) {
  //       loadedIngredients.push({
  //         id: key,
  //         title: responseData[key].title,
  //         amount: responseData[key].amount
  //       });
  //     }
  //     setUserIngredients(loadedIngredients);
  //   });
  // }, []); // [] dependencie => when it should rerender

  // useEffect(() => { // on every rerender of ingredients ( 2 times - [] and then when fetch ingredients)
  //   console.log('RENDERING INGREDIENTS', userIngredients);
  // }, [userIngredients]) // when userIngredients change

  useEffect(() => {
    if(!isLoading && !error && reqIndentifier === 'REMOVE_INGREDIENT')
      dispatch({ type: 'DELETE', id: reqExtra });
    else if (!isLoading && !error && reqIndentifier === 'ADD_INGREDIENT')
      dispatch({ type: 'ADD', ingredient: { id: data.name, ...reqExtra }});

  }, [data, reqExtra, reqIndentifier, isLoading, error])

  const addIngredientHandler = useCallback(ingredient => {
    sendRequest(
      'https://react-hooks-update-c3e42.firebaseio.com/ingredients.json',
      'POST',
      JSON.stringify(ingredient),
      ingredient,
      'ADD_INGREDIENT'
    );
    // dispatchHttp({ type: 'SEND' });
    // fetch('https://react-hooks-update-c3e42.firebaseio.com/ingredients.json', {
    //   method: 'POST',
    //   body: JSON.stringify(ingredient),
    //   headers: { 'Content-Type': 'application/json' }
    // }).then(response => {
    //   dispatchHttp({ type: 'RESPONSE' });
    //   return response.json();
    // }).then(responseData=> {
    //   dispatch({
    //     type: 'ADD',
    //     ingredient: {
    //       id: responseData.name,
    //       ...ingredient
    //     }
    //   })
    //   // setUserIngredients(prevIngredients => [
    //   //   ...prevIngredients,
    //   //   {
    //   //     id: responseData.name,
    //   //     ...ingredient
    //   //   }
    //   // ]);
    // }).catch(error => {
    //   dispatchHttp({ type: 'ERROR', error: error.message });
    // });
  }, [sendRequest]);

  const removeIngredientHanler = useCallback(id => {
    sendRequest(
      `https://react-hooks-update-c3e42.firebaseio.com/ingredients/${id}.json`,
      'DELETE',
      null,
      id,
      'REMOVE_INGREDIENT'
    );
    // dispatchHttp({ type: 'SEND' });
    // fetch(`https://react-hooks-update-c3e42.firebaseio.com/ingredients/${id}.json`, {
    //   method: 'DELETE'
    // }).then(response => {
    //   dispatch({
    //     type: 'DELETE',
    //     id: id
    //   })
    //   // setUserIngredients(prevIngredients => prevIngredients.filter(ingredient => ingredient.id !== id));
    //   dispatchHttp({ type: 'RESPONSE' });
    // }).catch(error => {
    //   dispatchHttp({ type: 'ERROR', error: error.message });
    // });
  }, [sendRequest]);

  const filteredIngredientsHandler = useCallback(filteredIngredients => {
    dispatch({
      type: 'SET',
      ingredients: filteredIngredients
    })
    // setUserIngredients(filteredIngredients);
  }, []);

  // const clearError = () => {
  //   // dispatchHttp({ type: 'CLEAR' });
  //   clear();
  // }

  const inredientList = useMemo(() => { // memorise data that we don't want to recreate for each rerender cycle
    return (
      <IngredientList ingredients={userIngredients} onRemoveItem={removeIngredientHanler} />
    )
  }, [userIngredients, removeIngredientHanler])

  return (
    <div className="App">
      { error && <ErrorModal onClose={clear}>{ error }</ErrorModal> } {/* && => will show when if condition is true */}
      <IngredientForm addIngredient={addIngredientHandler} loading={isLoading} />

      <section>
        <Search onLoadIngredients={filteredIngredientsHandler} />
        { inredientList }
      </section>
    </div>
  );
}

export default Ingredients;
